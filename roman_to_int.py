VALUES = { 'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000 }

def solution(x):
	print recurse(x)


def biggest(rome):
	big = 'I'
	for c in rome:
		if VALUES[c] > VALUES[big]:
			big = c
	return big

def recurse(rome):
	if len(rome) == 1:
		print "IF, current roman: {}".format(rome)
		return VALUES[rome]
	elif len(rome) == 0:
		return 0
	else:
		pos = rome.index(biggest(rome))
		return VALUES[rome[pos]] + recurse(rome[pos + 1:]) - recurse(rome[:pos])

solution('MCDXIV')
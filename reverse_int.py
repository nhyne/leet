i = 123

if i < 0:
	r = int('-' + str(i)[:0:-1])
else:
	r = int(str(i)[::-1])
if abs(r) > (2 ** 31 - 1):
	return 0

return r